import operator
import re
import logging
from collections import Counter, namedtuple, deque
import collections
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.layout import LAParams
from pdfminer.converter import PDFPageAggregator
from pdfminer.pdfdocument import PDFTextExtractionNotAllowed
from pdfminer.layout import LTChar, LTTextLine
import itertools

from pdfminer.pdfparser import PDFSyntaxError

log = logging.getLogger(__name__)

TextLine = namedtuple('TextElement', ['text', 'font_name', 'font_size'])

class FileAbstractExtractor:

    """Extract the abstract or executive summary from a PDF document.

    Arguments
    ----------

    max_pages : int
        Max number of pages to scan for an abstract or executive summary.

    Examples
    --------

    >>> import urllib.request
    >>> url = 'http://dergipark.gov.tr/download/article-file/419003'
    >>> urllib.request.urlretrieve(url, filename='SivasBasin.pdf')

    >>> pdf_doc = open('SivasBasin.pdf', 'rb')
    >>> FileAbstractExtractor(3).extract(pdf_doc)
    """

    _ABSTRACT_TITLES_WITHOUT_SPACES = ['abstract', 'summary', 'executivesummary']
    _SECTION_TITLES = ['introduction','methodology','acknowledgements','table of contents','references', 'keywords']

    def __init__(self, max_pages):
        self.max_pages = max_pages

    @staticmethod
    def _contains_section_title(text, titles):
        lower_text = text.lower()
        if any(map(lower_text.startswith, titles)):
            return True
        subsection_with_number = r'\d+\.\d*\s+({})'.format('|'.join(titles))
        if re.match(subsection_with_number, lower_text):
            return True
        text_without_whitespaces = re.sub('\s+', '', lower_text)
        if any(map(text_without_whitespaces.startswith, titles)):
            return True
        return False

    @staticmethod
    def _iter_all_char_elements(text_element):
        if isinstance(text_element, LTChar):
            yield text_element
        elements = deque()
        if isinstance(text_element, collections.Iterable):
            elements.extend(list(text_element))
        while elements:
            element = elements.popleft()
            if isinstance(element, collections.Iterable):
                elements.extend(list(element))
            elif isinstance(element, LTChar):
                yield element

    @staticmethod
    def _clean_whitespaces(text):
        return re.sub('\s+', ' ', text).strip()

    @staticmethod
    def _is_number(text):
        return text.strip().isdigit()

    @staticmethod
    def _get_most_common_element(iterable):
        counter = Counter()
        counter.update(iterable)
        return counter.most_common(1)[0][0]

    def _get_all_line_elements(self, text_element):
        if not isinstance(text_element, LTTextLine):
            return list(itertools.chain(self._get_all_line_elements(element) for element in text_element))
        return text_element

    def _detect_most_common_font(self, text_element):
        font_type_counter = Counter()
        font_size_counter = Counter()
        for char_element in self._iter_all_char_elements(text_element):
            font_type_counter.update([char_element.fontname.strip()])
            font_size_counter.update([round(char_element.size, 1)])
        most_common_font_name = font_type_counter.most_common(1)[0][0]
        most_common_font_size = font_size_counter.most_common(1)[0][0]
        return most_common_font_name, most_common_font_size

    def _contains_abstract_title(self, text):
        return self._contains_section_title(text, self._ABSTRACT_TITLES_WITHOUT_SPACES)

    def _contains_header_title(self, text):
        return self._contains_section_title(text, self._SECTION_TITLES)

    def _is_abstract_header(self, text):
        lower_text = text.lower()
        text_without_whitespaces = re.sub('\s+', '', lower_text)
        equals_abstract_title = (title == text_without_whitespaces for title in self._ABSTRACT_TITLES_WITHOUT_SPACES)
        return any(equals_abstract_title)

    def _iter_pdf_text_elements(self, file):
        rsrcmgr = PDFResourceManager()
        laparams = LAParams()
        device = PDFPageAggregator(rsrcmgr, laparams=laparams)
        interpreter = PDFPageInterpreter(rsrcmgr, device)

        page_generator = PDFPage.get_pages(file, maxpages=self.max_pages)
        try:
            for page in page_generator:
                interpreter.process_page(page)
                layout = device.get_result()
                for element in layout:
                    if hasattr(element, 'get_text'):
                        yield element
        except (PDFTextExtractionNotAllowed, PDFSyntaxError):
            log.warning(f'Cannot extract text from PDF')

    def _iter_pdf_filtered_text_lines(self, file):
        for text_element in self._iter_pdf_text_elements(file):
            for text_line in self._get_all_line_elements(text_element):
                stripped_text = text_line.get_text().strip()
                if stripped_text:
                    font, font_size = self._detect_most_common_font(text_line)
                    yield TextLine(text=stripped_text, font_name=font, font_size=font_size)

    def _detect_abstract_font(self, abstract_lines):
        lines = abstract_lines[:10]  # detect font and size based on first 10 lines
        font_names = map(operator.attrgetter('font_name'), lines)
        font_sizes = map(operator.attrgetter('font_size'), lines)
        abstract_font_name = self._get_most_common_element(font_names)
        abstract_font_size = self._get_most_common_element(font_sizes)
        return abstract_font_name, abstract_font_size

    def _clean_abstract_lines(self, abstract_lines):
        abstract_font_name, abstract_font_size = self._detect_abstract_font(abstract_lines)
        result_lines = []
        for abstract_line in abstract_lines:
            if abstract_line.font_name == abstract_font_name and \
               abstract_line.font_size == abstract_font_size:
                result_lines.append(abstract_line)
        return result_lines

    def _concat_abstract_lines(self, abstract_lines):
        abstract = ' '.join(abstract_line.text for abstract_line in abstract_lines)
        return self._clean_whitespaces(abstract)

    def extract(self, file):
        text_lines = self._iter_pdf_filtered_text_lines(file)
        for text_line in text_lines:
            if self._contains_abstract_title(text_line.text):
                break
        else:
            return
        if not self._is_abstract_header(text_line.text):
            text_lines = itertools.chain([text_line], text_lines)
        abstract_lines = []
        for text_line in text_lines:
            if self._contains_header_title(text_line.text):
                break
            abstract_lines.append(text_line)
        clean_lines = self._clean_abstract_lines(abstract_lines)
        return self._concat_abstract_lines(clean_lines)